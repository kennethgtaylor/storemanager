import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

class Backend {

  uid = '';
  storesRef = null;
  storeKey = null;
  latLang = null;
  formattedAddress = null;
  newStock = null;

  constructor () {
    firebase.initializeApp({
      apiKey: '---',
      authDomain: '---',
      databaseURL: '---',
      projectId: '---',
      messagingSenderId: '---',
    });

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.setUid(user.uid);
      } else {
        firebase.auth().signInAnonymously().catch((error) => {
            alert(error.message);
        });
      }

    });
  }
  setUid(value) {
    this.uid = value;
  }
  getUid() {
    return this.uid;
  }
  loadStores(callback) {
    this.storesRef = firebase.database().ref('stores');
    return this.storesRef;
  }
  addStore (storeName, storeAddress, callback) {
    this.storesRef = firebase.database().ref('stores');
    formattedAddress = storeAddress.split(' ').join('+');

    fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${storeAddress}`)
      .then((response) => response.json())
      .then((responseJson) => {
        latLang = responseJson.results[0].geometry.location;

        this.storesRef.push({
          _id: this.getUid(),
          name: storeName,
          address: storeAddress,
          lat: latLang.lat,
          lng: latLang.lng,
          stock: [],
          createdAt: Date.now(),
        });

      })
      .catch(error => console.log(error));

    
  }
  removeStore (key) {
    this.storesRef = firebase.database().ref('stores');

    this.storesRef.child(key).remove();
  }
  addStock (stockName, stockQuantity, storeName, stockType) {
    firebase.database().ref('stores').orderByChild('name').equalTo(storeName).on('value', (snap) => {
      this.storeKey = snap.node_.children_.root_.key;
    });
  
    firebase.database().ref(`stores/${this.storeKey}/stock`).push({
      stockName,
      stockQuantity,
      createdAt: Date.now(),
    });
    
    firebase.database().ref(`stores/${this.storeKey}/stock`).on('value', (snap) => {
      return newStock = snap.val();
    });
  }
  signUserIn() {
    
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.setUid(user.uid);;
        Actions.home();
      } else {
        firebase.auth().signInAnonymously()
          .then(() => {
            Actions.home();
          })
          .catch((error) => {
            alert(error.message);
          });
      }
    });
  }
}

export default new Backend();