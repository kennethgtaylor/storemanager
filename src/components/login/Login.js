import React, { Component } from 'react';

import {
  Button,
  View,
  TextInput,
  StyleSheet,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import Backend from '../../Backend';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
    };

    this._updateEmail = this._updateEmail.bind(this);
    this._updatePassword = this._updatePassword.bind(this);
    this._handleRegister = this._handleRegister.bind(this);
    this._handleSignIn = this._handleSignIn.bind(this);
    this._goToHome = this._goToHome.bind(this);
  }
  _updateEmail (text) {
    this.setState({email: text})
  }
  _updatePassword (text) {
    this.setState({password: text})
  }
  _handleRegister (e) {
    console.log(e);
  }
  _handleSignIn (e) {
    Backend.signUserIn();
  }

  _goToHome() {
    Actions.home();
  }

  render() {
    return (
      <View style={styles.container} >
        <TextInput 
          style={styles.input}
          placeholder="Company Email"
          onChangeText={this._updateEmail}
          value={this.state.email} />
        <TextInput 
          style={styles.input}
          placeholder="Password"
          onChangeText={this._updatePassword}
          value={this.state.password} />
        <Button
          onPress={this._handleSignIn}
          color="blue"
          title="Sign In" />
        <Button
          onPress={this._handleRegister}
          title="Register" />
      </View>
    );
  }
}

export default Login

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',

  },
  input: {
    height: 40,
    margin: 5,
    padding: 3,
    borderColor: 'grey',
    borderWidth: 2,
  }
});