import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native'

const PresentationalComponent = (props) => {
  return (
    <View>
      <Text 
        style={styles.myState}
        onPress={props.updateState}>
        {props.content}
      </Text>
    </View>
    );
}

export default PresentationalComponent

const styles = StyleSheet.create({
  myState: {
    margin: 30,
    textAlign: 'center',
    color: 'purple',
    fontWeight: 'bold',
    fontSize: 20
  }
});