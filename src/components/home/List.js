import React, { Component } from 'react';
import { Text, TouchableOpacity, StyleSheet, View } from 'react-native';

class List extends Component {
  state = {
    names: [
      {
        id: 0,
        name: 'Kenneth'
      },
      {
        id: 1,
        name: 'George'
      },
      {
        id: 2,
        name: 'Taylor'
      }
    ]
  };

  alertItemName = (item) => {
    alert(item.name);
  }

  render() {
    return (
      <View>
        {
          this.state.names.map((item, index) => (
            <TouchableOpacity
              key={item.id}
              style={styles.container}
              onPress={() => this.alertItemName(item)}>
              <Text style={styles.text}>
                {item.name}
              </Text>
            </TouchableOpacity>
          ))
        }
      </View>
    );
  }
}

export default List

const styles = StyleSheet.create({
  container: {
    padding: 10,
    marginTop: 3,
    backgroundColor: '#d9f9b1',
    alignItems: 'center'
  },
  text: {
    color: 'purple'
  }
});