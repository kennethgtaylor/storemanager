import React, { Component } from 'react';
import { 
  TouchableOpacity, 
  Text,
  StyleSheet,
  View,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

class Home extends Component {
  constructor() {
    super();
  }

  _goToStores() {
    Actions.stores();
  }

  _goToPlanner() {
    Actions.planner();
  }

  render() {
    return (
      <View style={styles.main}>
        <View style={styles.stores}>
          <Text style={styles.text} onPress={this._goToStores}>Stores</Text>
        </View>
        <View style={styles.planner}>
          <Text style={styles.text} onPress={this._goToPlanner}>Daily Planner</Text>
        </View>
      </View>
    );
  }
}

export default Home

const styles = StyleSheet.create({
  main: {
    flex: 1,
    flexDirection: 'column',
  },
  stores: {
    backgroundColor: 'maroon',
    justifyContent: 'space-around',
    flex: 1,
    alignItems: 'center',
  },
  planner: {
    backgroundColor: 'grey',
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
  },
  text: {
    color: 'white',
    fontSize: 50,
    fontWeight: 'bold',
  }
});