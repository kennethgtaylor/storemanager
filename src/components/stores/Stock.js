import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

import { Actions } from 'react-native-router-flux';

class Stock extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      hasStock: false,
    };
    
    this.goToAddStock = this.goToAddStock.bind(this);
  }
  componentWillMount() {
    if (this.props.stock) {
      console.log('Old Stock: ', this.props.stock);
      this.setState({
        hasStock: true
      });
    }
  }
  goToAddStock() {
    Actions.addstock({
      addStock: this.props.addStockItem,
      storeName: this.props.storeName,
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Stock</Text>
        <ScrollView style={styles.stockContainer}>
        { this.state.hasStock ? (
            // Object.keys(this.props.stock).length > 0 &&
            Object.keys(this.props.stock).map((key) => {
              return (
                <Text 
                  key={key}
                  style={styles.input}>
                  {this.props.stock[key].stockName} - {this.props.stock[key].stockQuantity}
                </Text>
              )
            })
          ) : (
            <Text>No Stock</Text>
        )}
        </ScrollView>
        <TouchableOpacity
          style={styles.addStock}
          onPress={this.goToAddStock}>
          <Text style={styles.addStockTitle}>+ Add Stock</Text>
        </TouchableOpacity>
        {/*
        TODO: 
        Fix spacing issues at the bottom of the container.
        Currently the last item is slightly cutoff.
        Get dummy data to make stock dynamic.
      */}
      </View>
    );
  }
}

export default Stock

const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
  },
  input: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    margin: 2,
    paddingTop: 7,
    height: 40,
    paddingLeft: 7,
    borderBottomWidth: .5,
    borderColor: '#000',
    backgroundColor: 'maroon',
  },
  stockContainer: {
    height: 135,
    marginTop: 5,
    paddingTop: 10,
  },
  addStock: {
    marginTop: 5,
  },
  addStockTitle: {
    fontSize: 12,
    marginTop: 10,
    fontWeight: 'bold',
    color: 'maroon',
  },
  title: {
    color: 'maroon',
    fontWeight: 'bold',
  },
});