import React, { Component } from 'react';
import {
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  View,
  Picker,
} from 'react-native';

import { Actions } from 'react-native-router-flux';

class AddStock extends Component {
  constructor(props) {
    super(props);

    this.state = {
      itemName: '',
      itemQuantity: '',
      itemType: 'Select Quantity Type',
    }

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleQuantityChange = this.handleQuantityChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleItemTypeChange = this.handleItemTypeChange.bind(this);
  }
  handleNameChange(text) {
    this.setState({
      itemName: text
    });
  }
  handleQuantityChange(text) {
    this.setState({
      itemQuantity: text
    });
  }
  handleItemTypeChange(text) {
    this.setState({
      itemType: text,
    });
  }
  handleSubmit() {
    const name = this.state.itemName;
    const quantity = this.state.itemQuantity;
    const itemType = this.state.itemType;

    this.props.addStock(name, quantity, this.props.storeName);
    Actions.pop({ refresh: {stock: newStock}});
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Add Stock</Text>
        <TextInput 
          style={styles.textInput} 
          placeholder="Item Name"
          onChangeText={this.handleNameChange}/>
        <TextInput 
          style={styles.textInput} 
          placeholder="Quantity"
          onChangeText={this.handleQuantityChange}/>

        <TouchableOpacity 
          onPress={this.handleSubmit} 
          style={styles.submitButton}>
          <View>
            <Text style={styles.content} >ADD STOCK ITEM</Text>
          </View>
        </TouchableOpacity>  
        {/*
          TODO:
          Style form
        */}
      </View>
    );
  }
}

export default AddStock

const styles = StyleSheet.create({
  container: {
    margin: 20,
  },
  title: {
    fontWeight: 'bold',
    color: 'maroon',
  },
  textInput: {
    marginTop: 10,
    marginBottom: 10,
    paddingBottom: 3,
    borderBottomWidth: 0.5,
    borderBottomColor: '#000',
  },
  submitButton: {
    backgroundColor: 'maroon',
    height: 40,
  },
  content: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    paddingTop: 10,
  }
});