import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
} from 'react-native';
import MapView from 'react-native-maps';
import { Actions } from 'react-native-router-flux';
import Stock from './Stock';

class Store extends Component {

  constructor(props) {
    super(props);
    
    storeLat = this.props.locationInfo.lat;
    storeLng = this.props.locationInfo.lng;

    this.state = {
      notes: '',
      stock: this.props.locationInfo.stock,
    };
  }
  componentWillReceiveProps(nextProps){
    console.log(nextProps.stock);

    this.setState({stock: nextProps.stock});
  }
  render () {
    return (
      <KeyboardAvoidingView 
        behavior="position"
        keyboardVerticalOffset={100}
        style={styles.container} >
        <ScrollView>
          <View style={styles.contentGroup}>
            <Text style={styles.title}>Store Name</Text>
            <Text style={styles.content}>{this.props.locationInfo.name}</Text>
          </View>
          <View style={styles.contentGroup}>
            <Text style={styles.title}>Store Address</Text>
            <Text style={styles.content}>{this.props.locationInfo.address}</Text>
          </View>
          <View style={styles.relativeContainer}>
            <MapView 
            style={styles.maps}
            initialRegion={{
              latitude: storeLat,
              longitude: storeLng,
              latitudeDelta: 0.01,
              longitudeDelta: 0.01,
            }}>
              <MapView.Marker 
                title={this.props.locationInfo.name}
                description="Store description"
                coordinate={{
                  latitude: storeLat,
                  longitude: storeLng,
                }}/>
            </MapView>
          </View>
          <Stock 
            addStockItem={this.props.addStock} 
            storeName={this.props.locationInfo.name}
            stock={this.state .stock} />
          <Text style={styles.title}>Store Notes</Text>
          <KeyboardAvoidingView
            behavior="padding">
            <TextInput 
            style={styles.title, styles.notesContainer}
            onChangeText={(text) => this.setState({ notes: text })}
            multiline={true}
            numberOfLine={2}
            placeholder="Notes..."/>
          </KeyboardAvoidingView>
          <TouchableOpacity
            style={styles.btn}
            onPress={() => {
              console.log('Delete Store');
            }}>
            <Text style={styles.btnText}
              onPress={() => {
                console.log('Delete Store')
                Actions.pop();
              }}>Delete Store</Text>
          </TouchableOpacity>
          {/*
            TODO: 
            Add make Google map dynamic.
            Add marker for store location.
            Add KeyboardAvoidView.
            Add deleteStock function.
            Add deleteSttore function.
            Figure out how to structure notes.
          */}
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

export default Store 

const styles = StyleSheet.create({
  btn: {
    backgroundColor: 'maroon',
    marginTop: 75,
    height: 40,
    paddingTop: 10,
  },
  btnText: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 15,
  },
  notesContainer: {
    borderBottomWidth: .5,
    // height: 150,
    color: 'black',
  },
  relativeContainer: {
    position: 'relative',
    height: 150,
    marginBottom: 20,
  },
  container: {
    // backgroundColor: '#9f9f9f',
    flex: 1,
    padding: 20
  },
  contentGroup: {
    marginBottom: 20,
  },
  title: {
    color: 'maroon',
    fontWeight: 'bold',
  },  
  content: {
    color: '#202020',
    fontSize: 25,
  },
  maps: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    overflow: 'hidden',
  }
});