import React, { Component } from 'react';
import {
  Button,
  ListView,
  Text,
  TextInput,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import Backend from '../../Backend';
import AddStore from './AddStore';

import { Actions } from 'react-native-router-flux';

class Stores extends Component {

  constructor() {
    super();
    const storeKey = null;
    this.state = {
      locations: [],
      storeName: '',
      storeAddress: '',
    }

    this._pickStore = this._pickStore.bind(this);
    this._handleNameChange = this._handleNameChange.bind(this);
    this._handleAddressChange = this._handleAddressChange.bind(this);
    this._handleStoreSubmit = this._handleStoreSubmit.bind(this);
    this._loadStores = this._loadStores.bind(this);
    this._goToStore = this._goToStore.bind(this);
    this._addStock = this._addStock.bind(this);
  }

  _loadStores() {
    Backend.loadStores().on('value', (snapshot) => {
      this.setState({ locations: snapshot.val() });
    });
  }
  componentDidMount() {
    this._loadStores();
  }
  _pickStore(e) {
    console.log(e.target);
  }
  _handleNameChange(storeName) {
    this.setState({ storeName: storeName });
  }
  _handleAddressChange(storeAddress) {
    this.setState({ storeAddress: storeAddress });
  }
  _handleStoreSubmit(){
    var storeName = this.state.storeName;
    var storeAddress = this.state.storeAddress;
    Backend.addStore(storeName, storeAddress);

    this.setState({
      storeName: '',
      storeAddress: '',
    });
  }
  _addStock(name, quantity, key) { 
    Backend.addStock(name, quantity, key);
  }
  _goToStore(locationInfo, storeKey) {
    Actions.store({
      storeKey,
      locationInfo,
      addStock: this._addStock,
    });
  }
  render() {
    return (
      <View style={styles.container}>
       <ScrollView style={styles.listContainer}>
        {
          Object.keys(this.state.locations).map((key) => {
            return (
              <TouchableOpacity
                style={styles.item}
                onPress={() => {
                  storeName = this.state.locations[key].name;
                  this._goToStore(this.state.locations[key], this._addStock, storeName);
                }}
                key={key}
              >
                <Text
                  style={styles.text}
                >{this.state.locations[key].name}</Text>
              </TouchableOpacity>
            )
          })
        }
        
       </ScrollView>
       <AddStore 
        style={styles.formContainer}
        nameChange={this._handleNameChange} 
        addressChange={this._handleAddressChange}
        submitStore={this._handleStoreSubmit}
        storeName={this.state.storeName}
        storeAddress={this.state.storeAddress}
      />
      </View>
      
    );
  }
}

export default Stores;

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    flex: 1,
    flexDirection: 'column'
  },
  listContainer: {
    flex: 7,
  },
  formContainer: {
    flex: 5,
  },
  text: {
    textAlign: 'center',
    fontSize: 15,
    padding: 10,
    color: 'white',
  },
  item: {
    padding: 5,
    margin: 1,
    backgroundColor: 'maroon',
  }
});