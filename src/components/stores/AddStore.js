import React, { Component } from 'react';
import {
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  View,
} from 'react-native';

const AddStore = (props) => {
  return (
    <View>
      <Text style={styles.title}>Add Store</Text>
      <TextInput 
        style={styles.input}
        onChangeText={props.nameChange}
        placeholder="Store Name"
        value={props.storeName}
      />
      <TextInput
        style={styles.input}
        onChangeText={props.addressChange}
        placeholder="Store Address"
        value={props.storeAddress}
      />

      <TouchableOpacity onPress={props.submitStore} >
        <View>
          <Text style={styles.submitButton}>ADD STORE</Text>
        </View>
      </TouchableOpacity>
    </View>
  )
}

export default AddStore

const styles = StyleSheet.create({
  container: {
    margin: 15,
  },
  title: {
    marginBottom: 1,
    marginTop: 25,
    fontSize: 20,
    fontWeight: 'bold',
  },
  input: {
    height: 40,
    borderWidth: 1,
    borderColor: 'grey',
    margin: 5,
  },
  submitButton: {
    backgroundColor: 'maroon',
    textAlign: 'center',
    paddingTop: 10,
    // marginLeft: 50,
    // marginRight: 50,
    height: 40,
    color: 'white',
    fontWeight: 'bold',
  }
});