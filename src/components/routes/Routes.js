import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import Home from '../home/Home';
import Login from '../login/Login';
import Stores from '../stores/Stores';
import Store from '../stores/Store';
import AddStock from '../stores/AddStock';

const Routes = () => (
  <Router>
    <Scene key="root">
      <Scene key="home" component={Home} title="Home"  />
      <Scene key="login" component={Login} title="Login" initial={true}/>
      <Scene key="store" component={Store} title="Store Info." />
      <Scene key="stores" component={Stores} title="Your Stores" />
      <Scene key="addstock" component={AddStock} title="Stock Management" />
    </Scene>
  </Router>
);

export default Routes