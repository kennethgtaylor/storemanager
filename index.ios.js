import React, { Component } from 'react';
import { AppRegistry, View } from 'react-native';
import Routes from './src/components/routes/Routes.js'

class SalesApp extends Component {
   render() {
      return (
         <Routes />
      );
   }
}
export default SalesApp

AppRegistry.registerComponent('SalesApp', () => SalesApp)