import React, { Component } from 'react';
import { AppRegistry, View } from 'react-native';
import Routes from './src/components/routes/Routes';

class SalesApp extends Component {
  render() {
    return (
      <Routes />
    );
  }
}

AppRegistry.registerComponent('SalesApp', () => SalesApp);
